import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import AppRoutes from './routes';

import './semantic/dist/semantic.min.css';
import './App.css';

const App = () => {
  return (
    <BrowserRouter>
      <Route
        render={({ location }) => (
          <div className="App">
            <AppRoutes location={location} />
          </div>
        )}
      />
    </BrowserRouter>
  );
};

export default App;

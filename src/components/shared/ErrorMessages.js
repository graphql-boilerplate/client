import React from 'react';
import { PropTypes } from 'prop-types';
import { Message } from 'semantic-ui-react';
import { Spring } from 'react-spring';

const ErrorMessages = ({ errors }) => {
  return (
    <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
      {styles => (
        <Message negative style={styles}>
          <Message.List>
            {errors.map(error => (
              <Message.Item key={Math.random()}>{error.message}</Message.Item>
            ))}
          </Message.List>
        </Message>
      )}
    </Spring>
  );
};

ErrorMessages.propTypes = {
  errors: PropTypes.shape({
    message: PropTypes.string
  })
};

ErrorMessages.defaultProps = {
  errors: []
};

export default ErrorMessages;

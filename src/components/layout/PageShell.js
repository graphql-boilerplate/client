import React from 'react';
import { Container } from 'semantic-ui-react';
import styled from 'styled-components';
import AppHeader from 'components/layout/AppHeader';
import { Spring } from 'react-spring';

const PageShellDiv = styled.div``;
const ContentDiv = styled.div`
  margin-top: 5em;
`;

const PageShell = Page => {
  return props => (
    <PageShellDiv>
      <AppHeader />
      <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
        {styles => (
          <Container fluid as={ContentDiv} style={styles}>
            <Page {...props} />
          </Container>
        )}
      </Spring>
    </PageShellDiv>
  );
};

export default PageShell;

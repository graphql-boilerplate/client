import React from 'react';
import { Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const AppHeader = () => {
  return (
    <Menu secondary size="large">
      <Menu.Menu position="right">
        <Menu.Item as={Link} to="/signup" name="signup">
          Signup
        </Menu.Item>

        <Menu.Item as={Link} to="/login" name="login">
          Login
        </Menu.Item>
      </Menu.Menu>
    </Menu>
  );
};

export default AppHeader;

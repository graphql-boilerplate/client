import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { Menu } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';

class LoggedInAppHeader extends PureComponent {
  logoutUser = () => {
    const { history } = this.props;
    localStorage.removeItem('token');
    history.push('/login');
  };

  render() {
    return (
      <Menu secondary size="large">
        <Menu.Menu position="right">
          <Menu.Item name="logout" onClick={() => this.logoutUser()}>
            Log out
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

LoggedInAppHeader.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func
  })
};

LoggedInAppHeader.defaultProps = {
  history: {
    push: () => {}
  }
};

export default withRouter(LoggedInAppHeader);

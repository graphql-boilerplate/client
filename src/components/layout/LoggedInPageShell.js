import React from 'react';
import { Container } from 'semantic-ui-react';
import styled from 'styled-components';
import { Query } from 'react-apollo';
import { CURRENT_USER } from 'queries/users';
import { Redirect } from 'react-router-dom';
import LoggedInAppHeader from 'components/layout/LoggedInAppHeader';
import { Fetching } from 'components/shared/Fetching';
import { Spring } from 'react-spring';

const LoggedInPageShellDiv = styled.div``;
const ContentDiv = styled.div``;

const LoggedInPageShell = Page => {
  return props => (
    <Query query={CURRENT_USER}>
      {({ loading, error }) => {
        if (loading) {
          return <Fetching />;
        }

        const isNotAuthorized = error
          ? error.graphQLErrors.some(
            graphQLError => graphQLError.message === 'Not authorized'
          )
          : false;

        if (isNotAuthorized) {
          return <Redirect to="/login" />;
        }

        return (
          <LoggedInPageShellDiv>
            <LoggedInAppHeader />
            <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
              {styles => (
                <Container fluid as={ContentDiv} style={styles}>
                  <Page {...props} />
                </Container>
              )}
            </Spring>
          </LoggedInPageShellDiv>
        );
      }}
    </Query>
  );
};

export default LoggedInPageShell;

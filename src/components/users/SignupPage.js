import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from 'semantic-ui-react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Mutation } from 'react-apollo';
import { SIGNUP } from 'mutations/users';
import { CURRENT_USER } from 'queries/users';
import GoogleAuthentication from 'components/users/GoogleAuthentication';
import ErrorMessages from 'components/shared/ErrorMessages';

const SignupForm = styled.div``;

class SignupPage extends PureComponent {
  state = {
    name: '',
    email: '',
    password: ''
  };

  storeUserInfo = (authentication, cache) => {
    const { history } = this.props;

    localStorage.setItem('token', authentication.token);
    cache.writeQuery({
      query: CURRENT_USER,
      data: { currentUser: authentication.user }
    });
    history.push('/logged-in');
  };

  render() {
    const { name, email, password } = this.state;

    return (
      <SignupForm>
        <Grid
          textAlign="center"
          style={{ height: '100%' }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="blue" textAlign="center">
              Signup
            </Header>
            <Mutation mutation={SIGNUP}>
              {(signup, { error }) => (
                <Form
                  size="large"
                  onSubmit={e => {
                    e.preventDefault();
                    signup({
                      variables: {
                        name,
                        email,
                        password
                      }
                    });
                  }}
                >
                  <Segment stacked>
                    {error
                      && error.graphQLErrors.length > 0 && (
                        <ErrorMessages errors={error.graphQLErrors} />
                    )}
                    <Form.Input
                      fluid
                      icon="user"
                      iconPosition="left"
                      placeholder="Name"
                      value={name}
                      onChange={e => this.setState({ name: e.target.value })}
                    />
                    <Form.Input
                      fluid
                      icon="user"
                      iconPosition="left"
                      placeholder="E-mail address"
                      value={email}
                      onChange={e => this.setState({ email: e.target.value })}
                    />
                    <Form.Input
                      fluid
                      icon="lock"
                      iconPosition="left"
                      placeholder="Password"
                      type="password"
                      value={password}
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                    />
                    <Button.Group size="large">
                      <Button color="blue">Signup</Button>
                      <Button.Or />
                      <GoogleAuthentication
                        onSuccess={this.storeUserInfo}
                        buttonText="Sign up using Google"
                      />
                    </Button.Group>
                  </Segment>
                </Form>
              )}
            </Mutation>
            <Message>
              Already have an account?
              <Link to="/login">Login</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </SignupForm>
    );
  }
}

SignupPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func
  })
};

SignupPage.defaultProps = {
  history: {
    push: () => {}
  }
};

export default SignupPage;

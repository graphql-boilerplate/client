import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { withApollo } from 'react-apollo';
import { GOOGLE_AUTHENTICATION } from 'mutations/users';
import { GoogleLogin } from 'react-google-login';
import { withRouter } from 'react-router-dom';

class GoogleAuthentication extends PureComponent {
  handleGoogleLoginSuccess = async response => {
    const { client, onSuccess } = this.props;

    await client.mutate({
      mutation: GOOGLE_AUTHENTICATION,
      variables: {
        googleToken: response.tokenId
      },
      update: (cache, { data: { googleAuthentication } }) => {
        onSuccess(googleAuthentication, cache);
      }
    });
  };

  handleGoogleLoginFailure = () => {};

  render() {
    const { buttonText } = this.props;
    return (
      <GoogleLogin
        className="ui button google plus"
        clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
        onSuccess={this.handleGoogleLoginSuccess}
        onFailure={this.handleGoogleLoginFailure}
      >
        <Icon name="google" />
        {buttonText}
      </GoogleLogin>
    );
  }
}

GoogleAuthentication.propTypes = {
  buttonText: PropTypes.string,
  onSuccess: PropTypes.func
};

GoogleAuthentication.defaultProps = {
  buttonText: 'Log in using Google',
  onSuccess: () => {}
};

export default withRouter(withApollo(GoogleAuthentication));

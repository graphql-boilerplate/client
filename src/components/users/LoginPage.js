import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from 'semantic-ui-react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Mutation, withApollo } from 'react-apollo';
import { LOGIN } from 'mutations/users';
import { CURRENT_USER } from 'queries/users';
import GoogleAuthentication from 'components/users/GoogleAuthentication';
import ErrorMessages from 'components/shared/ErrorMessages';

const LoginForm = styled.div``;

class LoginPage extends PureComponent {
  state = {
    email: '',
    password: ''
  };

  storeUserInfo = (authentication, cache) => {
    const { history } = this.props;

    localStorage.setItem('token', authentication.token);
    cache.writeQuery({
      query: CURRENT_USER,
      data: { currentUser: authentication.user }
    });
    history.push('/logged-in');
  };

  render() {
    const { email, password } = this.state;

    return (
      <LoginForm>
        <Grid
          textAlign="center"
          style={{ height: '100%' }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="blue" textAlign="center">
              Login to your account
            </Header>
            <Mutation
              mutation={LOGIN}
              update={(cache, { data: { login } }) => {
                this.storeUserInfo(login, cache);
              }}
            >
              {(login, { error }) => (
                <Form
                  size="large"
                  onSubmit={e => {
                    e.preventDefault();
                    login({
                      variables: {
                        email,
                        password
                      }
                    });
                  }}
                >
                  <Segment stacked>
                    {error
                      && error.graphQLErrors.length > 0 && (
                        <ErrorMessages errors={error.graphQLErrors} />
                    )}
                    <Form.Input
                      fluid
                      icon="user"
                      iconPosition="left"
                      placeholder="E-mail address"
                      value={email}
                      onChange={e => this.setState({ email: e.target.value })}
                    />
                    <Form.Input
                      fluid
                      icon="lock"
                      iconPosition="left"
                      placeholder="Password"
                      type="password"
                      value={password}
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                    />
                    <Button.Group size="large">
                      <Button color="blue">Login</Button>
                      <Button.Or />
                      <GoogleAuthentication
                        onSuccess={this.storeUserInfo}
                        buttonText="Log in using Google"
                      />
                    </Button.Group>
                  </Segment>
                </Form>
              )}
            </Mutation>
            <Message>
              New to us?
              <Link to="/signup">Signup</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </LoginForm>
    );
  }
}

LoginPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func
  })
};

LoginPage.defaultProps = {
  history: {
    push: () => {}
  }
};

export default withApollo(LoginPage);

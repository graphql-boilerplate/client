import gql from 'graphql-tag';

export const SIGNUP = gql`
  mutation Signup($name: String!, $email: String!, $password: String!) {
    signup(data: { name: $name, email: $email, password: $password }) {
      token
      user {
        id
        name
        email
        picture
      }
    }
  }
`;

export const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        id
        name
        email
        picture
      }
    }
  }
`;

export const GOOGLE_AUTHENTICATION = gql`
  mutation GoogleAuthentication($googleToken: String!) {
    googleAuthentication(googleToken: $googleToken) {
      token
      user {
        id
        name
        email
        picture
      }
    }
  }
`;

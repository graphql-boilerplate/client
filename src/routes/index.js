import React from 'react';
import { PropTypes } from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import IndexPage from 'components/IndexPage';
import LoginPage from 'components/users/LoginPage';
import SignupPage from 'components/users/SignupPage';
import LoggedInIndexPage from 'components/LoggedInIndexPage';

import PageShell from 'components/layout/PageShell';
import LoggedInPageShell from 'components/layout/LoggedInPageShell';

const AppRoutes = props => {
  const { location } = props;

  return (
    <Switch location={location}>
      <Route exact path="/" component={IndexPage} />
      <Route path="/login" component={PageShell(LoginPage)} />
      <Route path="/signup" component={PageShell(SignupPage)} />

      <Route path="/logged-in" component={LoggedInPageShell(LoggedInIndexPage)} />
    </Switch>
  );
};

AppRoutes.propTypes = {
  location: PropTypes.shape({})
};

AppRoutes.defaultProps = {
  location: {}
};

export default AppRoutes;
